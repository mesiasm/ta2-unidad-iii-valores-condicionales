"""                           author: "Angel Morocho"
                       e-mail: "angel.m.morocho.c@unl.edu.ec"


Ejercicio 3: Escribe un programa que solicite una puntuación entre 0.0 y 1.0. Si la
             puntuación está fuera de ese rango, muestra un mensaje de error. Si la puntuación
             está entre 0.0 y 1.0, muestra la calificación usando la tabla siguiente:
             Puntuación Calificación
             >= 0.9     Sobresaliente
             >= 0.8     Notable
             >= 0.7     Bien
             >= 0.6     Suficiente
             < 0.6      Insuficiente """
while True:
    try:
        cal = float(input('Introduzca la puntuación '))
        if 0 <= cal <= 1:
            if cal >= 0.9:
                print('Sobresaliente')
            elif cal >= 0.8:
                print('Notable')
            elif cal >= 0.7:
                print('Bien')
            elif cal >= 0.6:
                print('Suficiente')
            elif cal < 0.6:
                print(' Insuficiente')
        else:
            print('Puntuacion incorrecta')
        break
    except ValueError:
        print('Puntuacion incorrecta, ingrese numeros')