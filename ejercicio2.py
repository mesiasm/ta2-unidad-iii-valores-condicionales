"""                                 author: "Angel Morocho"
                             e-mail: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 2: Reescribe el programa del salario usando try y except, de modo que el
             programa sea capaz de gestionar entradas no numéricas con elegancia, mostrando
             un mensaje y saliendo del programa. A continuación se muestran dos ejecuciones
             del programa:"""

try:
    horas = float(input('Introduzca las Horas:'))
    tarifa = float(input('Introduzca la Tarifa por hora:'))
    if horas >= 40:
        horas_xt = horas - 40
        tarifa_xt = (0.5 * tarifa) + tarifa
        salario = (tarifa_xt * horas_xt) + (horas * tarifa)
        print('Su salario es de: ', salario)
    else:
        salario = tarifa * horas
        print('Su salario es de: ', salario)
except:
    print('Error, por favor introduzca números')