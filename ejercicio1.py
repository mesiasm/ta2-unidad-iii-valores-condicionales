"""                           author: "Angel Morocho"
                       e-mail: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 1: Reescribe el programa del cálculo del salario para darle al empleado
             1.5 veces la tarifa horaria para todas las horas trabajadas que excedan de 40."""

while True:
    try:
        horas = int(input("ingrese el numero de horas laboradas: \n"))
        while True:
            try:
                tarifa = float(input("ingrese la tarifa por hora: \n "))
                if horas >= 40:
                    horas_xt = horas - 40
                    tarifa_xt = (0.5 * tarifa) + tarifa
                    salario = (tarifa_xt * horas_xt) + (horas * tarifa)
                    print('Su salario es de: ', salario)

                else:
                    salario = tarifa * horas
                    print('Su salario es de: ', salario)
                break
            except ValueError:
                print("Tarifa incorrecta: Ingrese numeros")
        break
    except ValueError:
        print("Hora incorrecta: Ingrese numeros")